#The goal of this tool is to merge connected polylines based on the angle between them.
#It was developed by Nathanael Le Voguer, with python 2.7 and ArcMap 10.5.

#Problem, unsolved for now : The tool will connect based on the minimum angle for the first line, there is a possibility that there is a better angle for the second line with a third line.
#One thing which needs to be tried is to sort the lines by length before iterating, therefore the longest ones would be merged first.
#Also, if we sort it after every merge, it could slightly modify the results.

import arcpy
arcpy.env.overwriteOutput = True

#Variables required : the workspace, the polyline shape file and the minimum angle chosen
inWorkspace = arcpy.GetParameterAsText(0)
inLines = arcpy.GetParameterAsText(1)
inAngle = arcpy.GetParameterAsText(2)
boolSplit = arcpy.GetParameter(3)
boolLength = arcpy.GetParameter(4)
outName = arcpy.GetParameterAsText(5)

#Set up the workspace and the shapefile to work on.
arcpy.env.workspace = inWorkspace
polyLines = inLines
spatialRef = arcpy.Describe(polyLines).spatialReference

#The minimum angle above which the lines will not be merged.
#We have to multiply it by 1000 to keep the precision of 3 decimals but also work with integers (necesary).
inMinAngle = int(inAngle)
minAngle = inMinAngle * 1000

#def addAngle
#This will calculate the angle between each lines and adds it to a new field in the table aswell as to a list which we will use to determine the smallest angle between several lines.
#Angle are calculated between the two extreme points from each line to a North-South axis then added or substracted to each other.
#This way, it calculates the precise angle between the two lines.
#Therefore, if the line has several edges, it will use a straight line between the two extremities to calculate the angle.

def addAngle (firstAngle, secondAngle):
    if (firstAngle > 0 and secondAngle > 0) or (firstAngle <0 and secondAngle <0):
        if abs(firstAngle) > abs(secondAngle):
            angle = round(abs(firstAngle) - abs(secondAngle), 3)
        else:
            angle = round(abs(secondAngle) - abs(firstAngle), 3)
    else:
        if (abs(firstAngle) + abs(secondAngle)) < 180:
            angle = round(abs(firstAngle) + abs(secondAngle), 3)
        else:
            angle = 360 - round(abs(firstAngle) + abs(secondAngle), 3)
    tempAngle = 180 - angle 
    angleMinus = int(tempAngle * 1000)
    if angleMinus < minAngle:
        row[1] = angleMinus
        update.updateRow(row)
        minDirList.append(angleMinus)
        resetList.append(angleMinus)
        
#Prepare the data
#first, we separate the lines at each vertex if needed and crete a feature layer : do you want to split the input lines at each vertices ?
if boolSplit == True:
    arcpy.SplitLine_management(polyLines, "linesSplit.shp")
    tempLines = "linesSplit.shp"
else:
    arcpy.CopyFeatures_management(polyLines, "workingLines.shp")
    tempLines = "workingLines.shp"

#If required, we sort the lines by decreasing length, so that the longest ones are merged first
if boolLength == True:
    arcpy.AddGeometryAttributes_management(tempLines, "LENGTH", "METERS",)
    arcpy.Sort_management(tempLines, "sortedLines.shp", [["LENGTH", "DESCENDING"]])
    sortLines="sortedLines.shp"
    arcpy.MakeFeatureLayer_management("sortedLines.shp", "linesFL")
    lines = "linesFL"
    countSort = 0
    insertFields = ("SHAPE@", "DirMin", "LENGTH")
else:
    arcpy.MakeFeatureLayer_management(tempLines, "linesFL")
    lines = "linesFL"
    insertFields = ("SHAPE@", "DirMin")

#Then we add the DirMin field which will be used to store the calculated angles.
arcpy.AddField_management(lines, "DirMin", "LONG" )
arcpy.CalculateField_management(lines, "DirMin", "410000", "PYTHON")

#Create an "empty" list to add the angles to so that we can be sure to always get the minimum angle between several lines. 
minDirList = [400000]

#Cerate an empty list to decide at the end whether to reset the first cursor or not.
resetList = [400000]

#main
#First cursor : looks into each row and get the geometry of the line and first and last point.
while True:
    resetList = [400000]
    with arcpy.da.UpdateCursor(lines, ("SHAPE@", "DirMin", "OID@")) as cursor:
        for row in cursor:
            firstLine = row[0]
            startPoint = firstLine.firstPoint
            endPoint = firstLine.lastPoint
            startPointGeo = arcpy.PointGeometry(startPoint, spatialRef)
            endPointGeo = arcpy.PointGeometry(endPoint, spatialRef)
            #We select only the lines that have boundaries touching the first one, to make the second cursor much quicker.
            OID = row[2]
            selectCurrLine = '"' + "FID" + '"=' + str(OID)
            arcpy.SelectLayerByAttribute_management(lines, "NEW_SELECTION", selectCurrLine)
            arcpy.SelectLayerByLocation_management(lines, "BOUNDARY_TOUCHES", lines, "", "NEW_SELECTION")
            #Second cursor : looks into each selected row and fetch the geometry of a second line and the first and last point of this line.
            with arcpy.da.UpdateCursor(lines, ("SHAPE@", "DirMin")) as update:
                for row in update:
                    secondLine = row[0]
                    startPoint2 = secondLine.firstPoint
                    endPoint2 = secondLine.lastPoint
                    startPoint2Geo = arcpy.PointGeometry(startPoint2, spatialRef)
                    endPoint2Geo = arcpy.PointGeometry(endPoint2, spatialRef)
                    #Condition : if two points are connected (start or end), calculate the angle between the two lines
                    #The conditions make sure that the four posible connexion types are covered
                    if endPoint.contains(endPoint2) and not secondLine.equals(firstLine):
                        angleDist1 = endPointGeo.angleAndDistanceTo(startPointGeo, "GEODESIC")
                        angleDist2 = endPoint2Geo.angleAndDistanceTo(startPoint2Geo, "GEODESIC")
                        addAngle(angleDist1[0], angleDist2[0])
                    elif endPoint.contains(startPoint2) and not secondLine.equals(firstLine):
                        angleDist1 = endPointGeo.angleAndDistanceTo(startPointGeo, "GEODESIC")
                        angleDist2 = startPoint2Geo.angleAndDistanceTo(endPoint2Geo, "GEODESIC")
                        addAngle(angleDist1[0], angleDist2[0])
                    elif startPoint.contains(endPoint2) and not secondLine.equals(firstLine):
                        angleDist1 = startPointGeo.angleAndDistanceTo(endPointGeo, "GEODESIC")
                        angleDist2 = endPoint2Geo.angleAndDistanceTo(startPoint2Geo, "GEODESIC")
                        addAngle(angleDist1[0], angleDist2[0])
                    elif startPoint.contains(startPoint2) and not secondLine.equals(firstLine):
                        angleDist1 = startPointGeo.angleAndDistanceTo(endPointGeo, "GEODESIC")
                        angleDist2 = startPoint2Geo.angleAndDistanceTo(endPoint2Geo, "GEODESIC")
                        addAngle(angleDist1[0], angleDist2[0])
                #reset the cursor so that it looks again in the rows which were just covered
                update.reset()
                #for each row, check if the two lines touch and if the mergeAngle is equal to the minimum angle from the list we appended with all the angles calculated above
                #When it does, we merge the lines, reset the list, delete the two previous lines, and reset the update cursor
                for row in update:
                    thirdLine = row[0]
                    mergeAngle = row[1]
                    if thirdLine.touches(firstLine) and mergeAngle == min(minDirList):
                        with arcpy.da.InsertCursor(lines, insertFields) as insert:
                            uniLine = firstLine.union(thirdLine)
                            if boolLength == True:
                                uniLength = uniLine.length
                                insert.insertRow([uniLine, 410000, uniLength])
                            else:
                                insert.insertRow([uniLine, 410000])
                        minDirList = [400000]
                        update.deleteRow()
                        cursor.deleteRow()
                update.reset()
            arcpy.SelectLayerByAttribute_management(lines, "CLEAR_SELECTION")
    out = min(resetList)
    if boolLength == True:
        prevSortLines = sortLines
        countSort+=1
        sortLines = "sortedLines" + str(countSort) + ".shp"
        arcpy.Sort_management(lines, sortLines, [["LENGTH", "DESCENDING"]])
        arcpy.Delete_management(prevSortLines)
        arcpy.MakeFeatureLayer_management(sortLines, "linesFL")
        lines = "linesFL"
    if out > minAngle:
        break
                
#In the end, delete the DirMin field, create the final lines and delete the temporary feature classes
arcpy.DeleteField_management(lines, "DirMin")
finalLines = outName
arcpy.SelectLayerByAttribute_management(lines, "CLEAR_SELECTION")
arcpy.CopyFeatures_management (lines, finalLines)
arcpy.Delete_management("workingLines.shp")
if boolSplit == True:
    arcpy.Delete_management("linesSplit.shp")
if boolLength == True :
    arcpy.Delete_management(sortLines)